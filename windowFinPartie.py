from PySide6.QtWidgets import QApplication, QMainWindow

from designer.code.ui_interfaceFinPartie import Ui_interfaceFinPartieDialog

class WindowFinPartie(QMainWindow):
      def __init__(self):
        super(WindowFinPartie, self).__init__()

        # ici on associe le .ui modélisé avec le designer
        self.uiFinPartie = Ui_interfaceFinPartieDialog()
        self.uiFinPartie.setupUi(self)
