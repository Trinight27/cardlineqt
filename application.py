import sys
from typing import List

from PySide6.QtWidgets import QApplication, QMainWindow, QTableWidgetItem, QListWidgetItem

from designer.code.ui_interface import Ui_interfacePartie
from metier.animal import Animal
from metier.fonctions import getLesAnimaux, getElemInListeAleatoire, getElemPrecedent,getElemSuivant



# from PyQt5.QtWidgets import QApplication, QMainWindow
from windowFinPartie import WindowFinPartie


class Window(QMainWindow):
    """Classe de l'application princpale

        Attributs d'instance
        --------------------
        ui :
            l'interface graphique de l'application

        """

    def __init__(self):
        super(Window, self).__init__()

        # ici on associe le .ui modélisé avec le designer
        self.ui = Ui_interfacePartie()
        self.ui.setupUi(self)
        self.windowPartieFin = WindowFinPartie()
        self.ui.jouerPushButton.clicked.connect(self.validation)
        self.__initialiserPartie()



    ##################################################################################################################
    ## METHODES INTERACTION VIEW/MODEL
    ## accèdent aux composants graphiques et au model
    ##################################################################################################################
    def __initialiserPartie(self):

        self.lesAnimauxPartie = getLesAnimaux()
        self.compteurPen = 0
        self.deckJoueurModel = []
        self.cardlineModel = []
        lAnimalDeStart = getElemInListeAleatoire(self.lesAnimauxPartie)
        self.cardlineModel.append(lAnimalDeStart)
        self.lesAnimauxPartie.remove(lAnimalDeStart)
        for tirageDeck in range(0,4):
            unAnimal = getElemInListeAleatoire(self.lesAnimauxPartie)
            self.deckJoueurModel.append(unAnimal)
            self.lesAnimauxPartie.remove(unAnimal)
        self.__peuplerVueCardline()
        self.__peuplerVueDeckJoueur()

        self.ui.cardlineTableWidget.setRowCount(len(self.cardlineModel))
        headers = ["Nom", "NomScientifique", "Taille"]
        self.ui.cardlineTableWidget.setColumnCount(len(headers))
        self.ui.cardlineTableWidget.setHorizontalHeaderLabels(headers)
        for compteur in range(len(self.cardlineModel)):
            self.ui.cardlineTableWidget.setItem(compteur, 0, QTableWidgetItem(self.cardlineModel[compteur].getNom()))
            self.ui.cardlineTableWidget.setItem(compteur, 1,
                                                QTableWidgetItem(self.cardlineModel[compteur].getNomScientifique()))
            self.ui.cardlineTableWidget.setItem(compteur, 2, QTableWidgetItem(str(self.cardlineModel[compteur].getTaille())))

    def __peuplerVueCardline(self) -> None:
        self.ui.cardlineTableWidget.clear()
        self.ui.spinBox.setMaximum(len(self.cardlineModel)+1)
        self.cardlineModel.sort()
        for compteur in range(len(self.cardlineModel)):
            self.ui.cardlineTableWidget.setItem(compteur, 0, QTableWidgetItem(self.cardlineModel[compteur].getNom()))
            self.ui.cardlineTableWidget.setItem(compteur, 1,
                                                QTableWidgetItem(self.cardlineModel[compteur].getNomScientifique()))
            self.ui.cardlineTableWidget.setItem(compteur, 2, QTableWidgetItem(str(self.cardlineModel[compteur].getTaille())))


    def __peuplerVueDeckJoueur(self) -> None:
        self.ui.deckJoueurListWidget.clear()
        for uneCatreDeck in self.deckJoueurModel:
            self.ui.deckJoueurListWidget.addItem(QListWidgetItem(uneCatreDeck.getNom()))

    def validation(self):
        print("clic sur jouer")

        cardSelected=self.__getCarteDeckJoueur(self.ui.deckJoueurListWidget.currentIndex().row())
        print(self.__getCarteDeckJoueur(self.ui.deckJoueurListWidget.currentIndex().row()).getNom())

        spinVal =self.ui.spinBox.value()
        print(self.ui.spinBox.value())

        if self.__testerInsertion(spinVal,cardSelected) == True:

            self.ui.messJoueurLabel.setText("Correct !!")
            self.__ajouterCarteCardline(cardSelected)
            self.__retirerCarteDeckJoueur(cardSelected)
            self.__peuplerVueDeckJoueur()
            self.__peuplerVueCardline()
        else:
            self.compteurPen += 1
            self.ui.nbPenLineEdit.setText(str(self.compteurPen))
            if self.compteurPen != 4:

                self.__ajouterCarteDeckJoueur(getElemInListeAleatoire(self.lesAnimauxPartie))
                self.__retirerCarteDeckJoueur(cardSelected)
                self.__peuplerVueDeckJoueur()
                self.ui.messJoueurLabel.setText("Mauvaise réponse !!")

            else:
                self.ui.messJoueurLabel.setText("Tu as perdu la partie !!")
                self.__terminerPartie(False)

    def __terminerPartie(self,quelFin):
        if quelFin == True:
            self.windowPartieFin.uiFinPartie.emoticonLabel.setPixmap("image/bravo.png")
            self.windowPartieFin.uiFinPartie.label.setText("Bravo !!")
            self.windowPartieFin.show()
        else:
            self.windowPartieFin.uiFinPartie.emoticonLabel.setPixmap("image/perdu.png")
            self.windowPartieFin.uiFinPartie.label.setText("Nombre max de pénalités atteint !\n La Partie est perdue \n mais vous pouvez rejouer :)")
            self.windowPartieFin.show()
        self.windowPartieFin.uiFinPartie.rejouerPushButton.clicked.connect(self.__initialiserPartie())
        self.windowPartieFin.uiFinPartie.quitterPushButton.clicked.connect(self.window().close)

    ##################################################################################################################
    ## METHODES METIER
    ## n'accèdent pas aux composants graphiques
    ##################################################################################################################

    def __testerInsertion(self, position: int, unAni: Animal) -> bool:
        """ Permet de vérifier si la position proposée est correcte.
        :param position:
        :param unAnimal:
        :return: True si la position proposée est exacte
        """
        animalPrec =getElemPrecedent(self.cardlineModel, position)
        animalSuiv =getElemSuivant(self.cardlineModel, position)

        ok = False

        if animalPrec:
            if animalSuiv:
                if animalPrec < unAni and unAni < animalSuiv:
                    ok = True
            else:
                # ajout en fin de liste : suiv n'existe pas
                if animalPrec < unAni:
                    ok = True
        else:
            # ajout en début de liste : prec n'existe pas
            if animalSuiv:
                if unAni < animalSuiv:
                    ok = True
        return ok

    ##################################################################################################################
    ## METHODES MANIPULANT LE MODEL
    # Le model est représenté par les deux attributs deckJoueurModel et cardlineModel
    ##################################################################################################################
    def __ajouterCarteCardline(self, unAnimal: Animal) -> None:

        self.cardlineModel.append(unAnimal)
        self.ui.cardlineTableWidget.setRowCount(len(self.cardlineModel))
        self.ui.spinBox.setMaximum(len(self.cardlineModel)+1)
        for compteur in range(len(self.cardlineModel)):

            self.ui.cardlineTableWidget.setItem(compteur+1, 0, QTableWidgetItem(unAnimal.getNom()))
            self.ui.cardlineTableWidget.setItem(compteur+1, 1,
                                                QTableWidgetItem(unAnimal.getNomScientifique()))
            self.ui.cardlineTableWidget.setItem(compteur+1, 2, QTableWidgetItem(str(unAnimal.getTaille())))


    def __ajouterCarteDeckJoueur(self, unAnimal:Animal) -> None:
        self.deckJoueurModel.append(unAnimal)

    def __getCarteDeckJoueur(self,indexCarte):
        return self.deckJoueurModel[indexCarte]

    def __retirerCarteDeckJoueur(self,unAnimal):
        self.deckJoueurModel.remove(unAnimal)
        if len(self.deckJoueurModel) == 0 :
            self.ui.messJoueurLabel.setText("Vous avez gagné !!")
            self.__terminerPartie(True)


## PROGRAMME PRINCIPAL ##
if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = Window()
    window.show()

    sys.exit(app.exec_())
