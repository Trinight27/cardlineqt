class Animal:

    def __init__(self, unNom : str, unNomScientifique : str, taille: int):
        """Constructeur de la classe
        :param unNom : pour valoriser le nom de l'animal
        :param unNomScientifique : pour valoriser le nom scientifique de l'animal
        :param taille : pour valoriser la taille de l'animal
        """
        self.setNom(unNom)
        self.setNomScientifique(unNomScientifique)
        self.setTaille(taille)

    def getNom(self) -> str:
        """Getter sur l'attribut nom
        :return:
        """
        return self.__nom

    def getTaille(self) -> int:
        """Getter sur l'attribut taille
        :return:
        """
        return self.__taille

    def getNomScientifique(self) -> str:
        """Getter sur l'attribut nomScientifique
        :return:
        """
        return self.__nomScientifique

    def setNom(self, valeur: str) -> None:
        """Setter sur l'attribut nom. Permet sa valorisation
        :param valeur: nom
        """
        self.__nom = valeur

    def setNomScientifique(self, valeur: str) -> None:
        """Setter sur l'attribut nom. Permet sa valorisation
        :param valeur: nom
        """
        self.__nomScientifique = valeur

    def setTaille(self, valeur: int) -> None:
        """Setter sur l'attribut taille. Permet sa valorisation
        :param valeur : la taille moyenne
        """
        self.__taille = valeur

    def __str__(self) -> str:
        return self.__nom

    def __lt__(self, other) -> bool:
        return self.getTaille() < other.getTaille()