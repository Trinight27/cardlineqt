# fonctions permettant la manipulation d'une liste d'animaux

from typing import List
import random

from metier.animal import Animal


def afficherListe(liste: List[Animal]) -> None:
    """
    Permet d'afficher le contenu d'une liste
    :param liste: liste d'animaux
    """
    for i in range(len(liste)):
        print(str(i) + " - " + str(liste[i]))

def getElemPrecedent(liste: List[Animal], pos: int) -> Animal:
    """
    Retourne l'element précédent la position donnée. None s'il n'existe pas (début de liste)
    :param liste: liste d'anilaux
    :param pos: position
    """
    prec = None
    if pos > 0:
        prec = liste[pos - 2]
    return prec


def getElemSuivant(liste: List[Animal], pos: int) -> Animal:
    """
    Retourne l'élement suivant la position donnée. None s'il n'existe pas (fin de liste)
    :param liste: liste d'animaux
    :param pos : entier
    """
    suiv = None
    if pos <= len(liste) - 1:
        suiv = liste[pos]
    return suiv


def getElemInListeAleatoire(liste: List[Animal]) -> Animal:
    """
    Renvoie un element tiré au sort dans la liste passée en paramètre
    :param liste: liste d'animaux

    :rtype: Animal
    """
    elem = liste[random.randint(0, len(liste) - 1)]
    return elem

def getLesAnimaux() -> List[Animal]:
    """
    Retourne l'ensemble des cartes
    :return:
    """
    return [Animal("L'éléphant d'Asie", "Elephas maximus", 6000),
                       Animal("Le lémurien", "Lemur catta", 420),
                       Animal("Le lapin de garenne", "Oryctolagus cuniculus", 420),
                       Animal("La gazelle", "Gazella gazella", 1150),
                       Animal("Le mouton", "Ovis aries", 1200),
                       Animal("Le morse", "Odobenus rosmarus", 3150),
                       Animal("La reine des fourmis noires", "Lasius niger", 9),
                       Animal("Le putois", "Mustela putorius", 450),
                       Animal("La hyène tachetée", "Crocuta crocuta", 1300),
                       Animal("Le tatou", "Dasupus novemcinctus", 430),
                       Animal("Le léopard", "Panthera pardus", 1400),
                       Animal("Le bison", "Bison bison", 3000),
                       Animal("Le coq", "Gallus gallus domesticus", 650),
                       Animal("L'homme", "Homo sapiens", 1720),
                       Animal("La raie manta", "Mobula birostris", 5000)]
