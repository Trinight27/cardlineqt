# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'interface.ui'
##
## Created by: Qt User Interface Compiler version 6.0.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_interfacePartie(object):
    def setupUi(self, interfacePartie):
        if not interfacePartie.objectName():
            interfacePartie.setObjectName(u"interfacePartie")
        interfacePartie.resize(1104, 899)
        font = QFont()
        font.setFamily(u"Source Code Pro Black")
        font.setPointSize(12)
        font.setBold(True)
        interfacePartie.setFont(font)
        interfacePartie.setToolTipDuration(0)
        self.formLayoutWidget = QWidget(interfacePartie)
        self.formLayoutWidget.setObjectName(u"formLayoutWidget")
        self.formLayoutWidget.setGeometry(QRect(80, 20, 961, 581))
        self.formLayout = QFormLayout(self.formLayoutWidget)
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setSizeConstraint(QLayout.SetNoConstraint)
        self.formLayout.setFieldGrowthPolicy(QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setRowWrapPolicy(QFormLayout.DontWrapRows)
        self.formLayout.setLabelAlignment(Qt.AlignCenter)
        self.formLayout.setFormAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.formLayout.setHorizontalSpacing(6)
        self.formLayout.setVerticalSpacing(6)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.cardlineLabel = QLabel(self.formLayoutWidget)
        self.cardlineLabel.setObjectName(u"cardlineLabel")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.cardlineLabel)

        self.cardlineTableWidget = QTableWidget(self.formLayoutWidget)
        if (self.cardlineTableWidget.columnCount() < 3):
            self.cardlineTableWidget.setColumnCount(3)
        __qtablewidgetitem = QTableWidgetItem()
        self.cardlineTableWidget.setHorizontalHeaderItem(0, __qtablewidgetitem)
        font1 = QFont()
        font1.setBold(False)
        __qtablewidgetitem1 = QTableWidgetItem()
        __qtablewidgetitem1.setFont(font1);
        self.cardlineTableWidget.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.cardlineTableWidget.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        self.cardlineTableWidget.setObjectName(u"cardlineTableWidget")
        self.cardlineTableWidget.setMaximumSize(QSize(700, 500))
        self.cardlineTableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.cardlineTableWidget.horizontalHeader().setMinimumSectionSize(35)
        self.cardlineTableWidget.horizontalHeader().setDefaultSectionSize(200)

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.cardlineTableWidget)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.formLayout.setItem(1, QFormLayout.FieldRole, self.verticalSpacer)

        self.deckJoueurListWidget = QListWidget(self.formLayoutWidget)
        self.deckJoueurListWidget.setObjectName(u"deckJoueurListWidget")
        self.deckJoueurListWidget.setEnabled(True)
        self.deckJoueurListWidget.setMaximumSize(QSize(700, 100))

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.deckJoueurListWidget)

        self.deckLabel = QLabel(self.formLayoutWidget)
        self.deckLabel.setObjectName(u"deckLabel")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.deckLabel)

        self.verticalSpacer_2 = QSpacerItem(1, 10, QSizePolicy.Minimum, QSizePolicy.MinimumExpanding)

        self.formLayout.setItem(3, QFormLayout.FieldRole, self.verticalSpacer_2)

        self.spinBox = QSpinBox(self.formLayoutWidget)
        self.spinBox.setObjectName(u"spinBox")
        self.spinBox.setMaximumSize(QSize(50, 16777215))

        self.formLayout.setWidget(4, QFormLayout.FieldRole, self.spinBox)

        self.placeLabel = QLabel(self.formLayoutWidget)
        self.placeLabel.setObjectName(u"placeLabel")

        self.formLayout.setWidget(4, QFormLayout.LabelRole, self.placeLabel)

        self.verticalLayoutWidget = QWidget(interfacePartie)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(369, 629, 671, 251))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.jouerPushButton = QPushButton(self.verticalLayoutWidget)
        self.jouerPushButton.setObjectName(u"jouerPushButton")
        self.jouerPushButton.setMaximumSize(QSize(200, 50))

        self.horizontalLayout_2.addWidget(self.jouerPushButton)

        self.messJoueurLabel = QLabel(self.verticalLayoutWidget)
        self.messJoueurLabel.setObjectName(u"messJoueurLabel")
        self.messJoueurLabel.setFrameShape(QFrame.NoFrame)
        self.messJoueurLabel.setFrameShadow(QFrame.Plain)

        self.horizontalLayout_2.addWidget(self.messJoueurLabel)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.penLabel = QLabel(self.verticalLayoutWidget)
        self.penLabel.setObjectName(u"penLabel")
        self.penLabel.setMaximumSize(QSize(250, 50))

        self.horizontalLayout_3.addWidget(self.penLabel)

        self.nbPenLineEdit = QLineEdit(self.verticalLayoutWidget)
        self.nbPenLineEdit.setObjectName(u"nbPenLineEdit")
        self.nbPenLineEdit.setMaximumSize(QSize(50, 30))

        self.horizontalLayout_3.addWidget(self.nbPenLineEdit)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.quitPushButton = QPushButton(self.verticalLayoutWidget)
        self.quitPushButton.setObjectName(u"quitPushButton")
        self.quitPushButton.setMaximumSize(QSize(150, 50))

        self.horizontalLayout.addWidget(self.quitPushButton)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.imageLabel = QLabel(interfacePartie)
        self.imageLabel.setObjectName(u"imageLabel")
        self.imageLabel.setGeometry(QRect(90, 640, 251, 221))
        self.imageLabel.setFrameShape(QFrame.NoFrame)
        self.imageLabel.setFrameShadow(QFrame.Plain)
        self.imageLabel.setPixmap(QPixmap(u"../../../../../../Pictures/Saved Pictures/NecessaryPertinentAsianconstablebutterfly-small.gif"))
        self.imageLabel.setScaledContents(True)

        self.retranslateUi(interfacePartie)
        self.quitPushButton.clicked.connect(interfacePartie.close)

        QMetaObject.connectSlotsByName(interfacePartie)
    # setupUi

    def retranslateUi(self, interfacePartie):
        interfacePartie.setWindowTitle(QCoreApplication.translate("interfacePartie", u"Cardline", None))
        self.cardlineLabel.setText(QCoreApplication.translate("interfacePartie", u"Cardline", None))
        ___qtablewidgetitem = self.cardlineTableWidget.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("interfacePartie", u"Nom", None));
        ___qtablewidgetitem1 = self.cardlineTableWidget.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("interfacePartie", u"Nom Scientifique", None));
        ___qtablewidgetitem2 = self.cardlineTableWidget.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("interfacePartie", u"Taille", None));
        self.deckLabel.setText(QCoreApplication.translate("interfacePartie", u"Choisir une carte \u00e0 jouer", None))
        self.placeLabel.setText(QCoreApplication.translate("interfacePartie", u"Choisir sa place dans la cardline", None))
        self.jouerPushButton.setText(QCoreApplication.translate("interfacePartie", u"Jouer", None))
        self.messJoueurLabel.setText(QCoreApplication.translate("interfacePartie", u"Message du joueur", None))
        self.penLabel.setText(QCoreApplication.translate("interfacePartie", u"Nombre de p\u00e9nalit\u00e9s", None))
        self.quitPushButton.setText(QCoreApplication.translate("interfacePartie", u"Quitter", None))
        self.imageLabel.setText("")
    # retranslateUi

