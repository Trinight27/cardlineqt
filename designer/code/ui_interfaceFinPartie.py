# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'interfaceFinPartie.ui'
##
## Created by: Qt User Interface Compiler version 6.0.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_interfaceFinPartieDialog(object):
    def setupUi(self, interfaceFinPartieDialog):
        if not interfaceFinPartieDialog.objectName():
            interfaceFinPartieDialog.setObjectName(u"interfaceFinPartieDialog")
        interfaceFinPartieDialog.resize(512, 407)
        self.verticalLayoutWidget = QWidget(interfaceFinPartieDialog)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(20, 60, 471, 281))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self.verticalLayoutWidget)
        self.label.setObjectName(u"label")
        font = QFont()
        font.setFamily(u"Source Code Pro Black")
        font.setPointSize(12)
        font.setBold(True)
        self.label.setFont(font)
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.label)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)

        self.emoticonLabel = QLabel(self.verticalLayoutWidget)
        self.emoticonLabel.setObjectName(u"emoticonLabel")
        self.emoticonLabel.setMaximumSize(QSize(100, 125))
        self.emoticonLabel.setFont(font)

        self.horizontalLayout_3.addWidget(self.emoticonLabel)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.rejouerPushButton = QPushButton(self.verticalLayoutWidget)
        self.rejouerPushButton.setObjectName(u"rejouerPushButton")
        self.rejouerPushButton.setMaximumSize(QSize(100, 50))
        self.rejouerPushButton.setFont(font)

        self.horizontalLayout.addWidget(self.rejouerPushButton)

        self.quitterPushButton = QPushButton(self.verticalLayoutWidget)
        self.quitterPushButton.setObjectName(u"quitterPushButton")
        self.quitterPushButton.setMaximumSize(QSize(100, 50))
        self.quitterPushButton.setFont(font)

        self.horizontalLayout.addWidget(self.quitterPushButton)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.retranslateUi(interfaceFinPartieDialog)
        self.rejouerPushButton.clicked.connect(interfaceFinPartieDialog.close)
        self.quitterPushButton.clicked.connect(interfaceFinPartieDialog.close)

        QMetaObject.connectSlotsByName(interfaceFinPartieDialog)
    # setupUi

    def retranslateUi(self, interfaceFinPartieDialog):
        interfaceFinPartieDialog.setWindowTitle(QCoreApplication.translate("interfaceFinPartieDialog", u"Form", None))
        self.label.setText(QCoreApplication.translate("interfaceFinPartieDialog", u"Message de fin de partie", None))
        self.emoticonLabel.setText(QCoreApplication.translate("interfaceFinPartieDialog", u"TextLabel", None))
        self.rejouerPushButton.setText(QCoreApplication.translate("interfaceFinPartieDialog", u"Rejouer", None))
        self.quitterPushButton.setText(QCoreApplication.translate("interfaceFinPartieDialog", u"Quitter", None))
    # retranslateUi

